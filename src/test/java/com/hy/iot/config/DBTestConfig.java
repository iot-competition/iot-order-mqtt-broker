package com.hy.iot.config;

import io.github.cdimascio.dotenv.Dotenv;
import lombok.val;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DBTestConfig extends DBConfig {

    private Dotenv env = Dotenv.configure().ignoreIfMissing().load();

    @Primary
    @Bean
    @Override
    public DataSource dataSource() {
        val dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.get("JDBC_CLASSNAME", "com.mysql.cj.jdbc.Driver"));
        dataSource.setUrl(env.get("JDBC_TEST_URL"));
        dataSource.setUsername(env.get("JDBC_USERNAME"));
        dataSource.setPassword(env.get("JDBC_PASSWORD"));
        return dataSource;
    }

}
