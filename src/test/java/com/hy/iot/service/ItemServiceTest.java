package com.hy.iot.service;

import com.hy.iot.config.DBTestConfig;
import com.hy.iot.entity.Item;
import com.hy.iot.entity.User;
import com.hy.iot.enums.UserItemStatus;
import com.hy.iot.enums.UserType;
import io.github.cdimascio.japierrors.ApiError;
import lombok.val;
import lombok.var;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest(
        properties = {
                "logging.level.root=OFF",
                "spring.main.banner-mode=OFF",
        }
)
@Import(value = {DBTestConfig.class, ItemService.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class ItemServiceTest {

    @Autowired private ItemService itemService;

    @Autowired private TestEntityManager entityManager;

    @Test
    public void add_order() throws ApiError {
        var item = new Item();
        item.setAmount(400);
        item.setType("Test Type");
        item.setName("Test Name");

        var user = new User();
        user.setName("Name");
        user.setCode("1111");
        user.setAge(20);
        user.setType(UserType.CLIENT);

        entityManager.persist(item);
        entityManager.persist(user);

        val order = itemService.addOrder(user.getId(), item.getId(), 10);
        Assert.assertNotNull(order);
        Assert.assertEquals(UserItemStatus.PENDING, order.getStatus());
        Assert.assertEquals(item.getId(), order.getItem().getId());
        Assert.assertEquals(item.getName(), order.getItem().getName());

        itemService.addOrder(user.getId(), item.getId(), 5);

        Assert.assertEquals(UserItemStatus.CANCELED, order.getStatus());
    }
}
