package com.hy.iot.service;

import com.hy.iot.config.DBTestConfig;
import com.hy.iot.entity.CheckHistory;
import com.hy.iot.entity.User;
import com.hy.iot.enums.UserType;
import io.github.cdimascio.japierrors.ApiError;
import lombok.val;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest(
        properties = {
                "logging.level.root=OFF",
                "spring.main.banner-mode=OFF",
        }
)
@Import(value = {DBTestConfig.class, UserService.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class UserServiceTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserService userService;

    private List<User> users = Arrays.asList(
            new User("1111", "Leo", UserType.STUDENT, Boolean.FALSE),
            new User("2222", "Lee", UserType.STUDENT, Boolean.FALSE)
    );

    @Before
    public void before() {
        users.forEach(entityManager::persist);
    }

    @After
    public void after() {
        entityManager.clear();
    }


    @Test
    public void test_get_all_user() {
        val page = userService.getAll(50, 0, null, null, null);

        Assert.assertNotNull(page);
        Assert.assertEquals(users, page.getContent());
        Assert.assertEquals(50 , page.getSize());
        Assert.assertEquals(0, page.getNumber());
    }

    @Test
    public void test_check_user_by_code() {
        val user = users.get(0);
        val history = new CheckHistory(user);
        entityManager.persist(user);
        entityManager.persist(history);

        val result = userService.checkByCode(user.getCode());

        Assert.assertNotNull(result);
        Assert.assertEquals(user, result);
        Assert.assertEquals(history.getUser(), user);
        Assert.assertTrue(result.getIsChecked());
    }


    @Test
    public void test_add_student() throws ApiError {
        val result = userService.addUser("Lii", "3333", UserType.STUDENT, null, null);
        Assert.assertNotNull(result);
        Assert.assertEquals(UserType.STUDENT, result.getType());
        Assert.assertFalse(result.getIsChecked());
    }
}
